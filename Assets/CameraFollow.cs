﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	public static CameraFollowState currentCameraFollowState;
	public Vector3 previousPosition;
	// Use this for initialization
	void Start () {
		currentCameraFollowState = CameraFollowState.NoFollowing;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		switch (currentCameraFollowState) {
		case CameraFollowState.NoFollowing:
			break;
		case CameraFollowState.StartFollowing:
			previousPosition = GameManager.currentBird.transform.position;
			currentCameraFollowState = CameraFollowState.Following;
			CameraMove.currentCameraMoveState = CameraMoveState.NoMoving;
			break;
		case CameraFollowState.Following:
			if(GameManager.currentBird != null) {
				float CameraDeltaX = GameManager.currentBird.transform.position.x - previousPosition.x,
				newCameraX = Mathf.Clamp (transform.position.x + CameraDeltaX,
				                          GetComponent<CameraAspect> ().CameraMinX,
				                          GetComponent<CameraAspect> ().CameraMaxX);
				GetComponent<CameraMove> ().UpdatePosition (newCameraX, transform.position.y, transform.position.z);
				previousPosition = GameManager.currentBird.transform.position;
			}
			break;
		}
	}
}
