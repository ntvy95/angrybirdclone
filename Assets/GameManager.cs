﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {
	public static GameState CurrentGameState;
	public static GameObject MainCamera;
	public static GameObject currentBird;
	//public static GameObject currentJumpingBird;
	public static Transform BirdThrowPosition;
	public static int currentBirdindex;
	//public static int currentJumpingBirdoffset;
	public static List<GameObject> Birds;
	public static int numberofPig;
	public static GameObject BirdTrail;
	public static GameObject BirdFur;
	public static float BirdVelocity;
	public static Vector3 currentdotTrailPosition;
	// Use this for initialization
	void Start () {
		CurrentGameState = GameState.MovingCameraBack;
		MainCamera = GameObject.Find ("Main Camera");
		BirdThrowPosition = GameObject.Find ("BirdThrowPosition").transform;
		BirdTrail = GameObject.Find ("BirdTrail");
		BirdFur = GameObject.Find ("BirdFur");
		Birds = new List<GameObject>(GameObject.FindGameObjectsWithTag("Bird"));

		currentBirdindex = 0;
		//currentJumpingBirdoffset = 1;
		currentBird = Birds[currentBirdindex];
		currentBird.GetComponent<Bird>().NotThrownYet = false;
		//currentJumpingBird = Birds[currentBirdindex + currentJumpingBirdoffset];
		//currentJumpingBird.GetComponent<Bird> ().isCurrentJump = true;

		numberofPig = (new List<GameObject>(GameObject.FindGameObjectsWithTag ("Pig"))).Count;
	}
	
	// Update is called once per frame
	void Update () {
		switch (CurrentGameState) {
		case GameState.MovingCameraBack:
			break;
		case GameState.Start:
			break;
		case GameState.StartThrowing:
			DeleteBirdTrail();
			initiatedotTrail();
			StartCoroutine (LeaveTrail ());
			CurrentGameState = GameState.Throwing;
			break;
		case GameState.Throwing:
			break;
		case GameState.EndThrowing:
			break;
		case GameState.End:
			if(numberofPig == 0) {
				//Win
			}
			else {
				//Lose
			}
			break;
		}
	}

	public static void SwitchBird() {
		CameraFollow.currentCameraFollowState = CameraFollowState.NoFollowing;
		CameraMove.currentCameraMoveState = CameraMoveState.AllowMoving;
		currentBirdindex = currentBirdindex + 1;
		if (currentBirdindex < Birds.Count) {
			CameraMove.currentMovingCameraBackState = MovingCameraBackState.MovingCameraBackStart;
			currentBird = Birds [currentBirdindex];
			currentBird.GetComponent<Bird>().BackToInitialState();
			currentBird.GetComponent<Bird>().NotThrownYet = false;
			SlingShot.currentSlingShotState = SlingShotState.NoBird;
			//GameManager.SwitchCurrentJumpingBird ();
		} else {
			CameraMove.currentMovingCameraBackState = MovingCameraBackState.MovingCameraBackEnd;
			SlingShot.currentSlingShotState = SlingShotState.End;
		}
	}

	/*public static void SwitchCurrentJumpingBird() {
		if (currentBirdindex < (Birds.Count - 1)) {
			currentJumpingBirdoffset = currentJumpingBirdoffset + 1;
			if (currentBirdindex + currentJumpingBirdoffset >= Birds.Count) {
				currentJumpingBirdoffset = 1;
			}
			currentJumpingBird = Birds [currentBirdindex + currentJumpingBirdoffset];
			currentJumpingBird.GetComponent<Bird>().isCurrentJump = true;
		} else {
			currentJumpingBird = null;
		}
	}*/

	IEnumerator LeaveTrail() {
		while (currentBird != null && CurrentGameState != GameState.EndThrowing) {
			if(Vector2.Distance (currentdotTrailPosition, currentBird.transform.position) >= 0.5f) {
				initiatedotTrail();
			}
			yield return null;
		}
	}

	public void initiatedotTrail()
	{
		GameObject dotTrail = (GameObject)Instantiate (BirdTrail, currentBird.transform.position, Quaternion.identity);
		dotTrail.tag = "BirdTrail Clone";
		dotTrail.GetComponent<SpriteRenderer> ().enabled = true;
		currentdotTrailPosition = dotTrail.transform.position;
	}

	public static void DeleteBirdTrail() {
		GameObject[] BirdTrailTrack = GameObject.FindGameObjectsWithTag ("BirdTrail Clone");
		for(int i = 0; i < BirdTrailTrack.Length; i++) {
			Destroy (BirdTrailTrack[i]);
		}
	}
}
