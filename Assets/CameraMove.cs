﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {
	[HideInInspector]
	public Vector3 previousPosition;
	public float dragSpeed;
	public static CameraMoveState currentCameraMoveState;
	public static MovingCameraBackState currentMovingCameraBackState;
	// Use this for initialization
	void Start () {
		currentCameraMoveState = CameraMoveState.AllowMoving;
		currentMovingCameraBackState = MovingCameraBackState.MovingCameraBackStart;
	}

	public void Update() {
		switch (currentMovingCameraBackState) {
		case MovingCameraBackState.NoMoving:
			break;
		case MovingCameraBackState.MovingCameraBackStart:
			StartCoroutine(CameraMoveBackToLeftEdgeAfter(2f, GameState.Start));
			break;
		case MovingCameraBackState.MovingCameraBackEnd:
			StartCoroutine(CameraMoveBackToLeftEdgeAfter(2f, GameState.End));
			break;
		}
	}

	public void LateUpdate() {
		switch(currentCameraMoveState){
		case CameraMoveState.NoMoving:
			break;
		case CameraMoveState.AllowMoving:
			if (Input.GetMouseButtonDown (0)) {
				previousPosition = Input.mousePosition;
				if (GameManager.CurrentGameState == GameState.MovingCameraBack) {
					if(currentMovingCameraBackState == MovingCameraBackState.MovingCameraBackStart) {
						GameManager.CurrentGameState = GameState.Start;
					}
					else {
						GameManager.CurrentGameState = GameState.End;
					}
					currentMovingCameraBackState = MovingCameraBackState.NoMoving;
				}
				return;
			}
			if (Input.GetMouseButton (0)) {
				float MouseDeltaX = Input.mousePosition.x - previousPosition.x;
				if (dragSpeed < 0.05f) {
					dragSpeed = dragSpeed + 0.0005f;
				}
				float CameraDeltaX = - (MouseDeltaX * dragSpeed),
				newCameraX = Mathf.Clamp (transform.position.x + CameraDeltaX,
				                          GetComponent<CameraAspect>().CameraMinX - Constants.CameraDeltaX,
				                          GetComponent<CameraAspect>().CameraMaxX + Constants.CameraDeltaX);
				UpdatePosition (newCameraX, transform.position.y, transform.position.z);
				previousPosition = Input.mousePosition;
			}
			if (transform.position.x < GetComponent<CameraAspect>().CameraMinX) {
				float newCameraX = transform.position.x + 0.15f;
				UpdatePosition (newCameraX, transform.position.y, transform.position.z);
			} else if (transform.position.x > GetComponent<CameraAspect>().CameraMaxX) {
				float newCameraX = transform.position.x - 0.15f;
				UpdatePosition (newCameraX, transform.position.y, transform.position.z);
			}
			break;
		}
	}

	public void UpdatePosition(float X, float Y, float Z) {
		transform.position = new Vector3 (X, Y, Z);
	}

	IEnumerator CameraMoveBackToLeftEdgeAfter(float seconds, GameState gamestate) {
		GameManager.CurrentGameState = GameState.MovingCameraBack;
		yield return new WaitForSeconds (seconds);
		while ((transform.position.x > GetComponent<CameraAspect>().CameraMinX)
		       && (GameManager.CurrentGameState == GameState.MovingCameraBack)) {
			float newCameraX = transform.position.x - 0.2f;
			UpdatePosition(newCameraX, transform.position.y, transform.position.z);
			yield return null;
		}
		if (GameManager.CurrentGameState == GameState.MovingCameraBack) {
			UpdatePosition (GetComponent<CameraAspect>().CameraMinX, transform.position.y, transform.position.z);
		}
		GameManager.CurrentGameState = gamestate;
		currentMovingCameraBackState = MovingCameraBackState.NoMoving;
	}
}
