using UnityEngine;
using System.Collections;

public class Wood : MonoBehaviour {
	public float maxHP;
	[HideInInspector]
	public float HP;
	[HideInInspector]
	public bool Destroyed;
	// Use this for initialization
	void Start () {
		HP = maxHP;
		Destroyed = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnCollisionEnter2D(Collision2D col) {
		if (Destroyed == false) {
			float Force = Vector2.Distance (col.relativeVelocity, new Vector2 (0, 0)),
			Damage = Force * 50f;
			HP = HP - Damage;
			if (HP <= 0) {
				Destroyed = true;
				GetComponent<AudioSource> ().Play ();
				GetComponent<SpriteRenderer>().enabled = false;
				Destroy (this.gameObject, GetComponent<AudioSource>().clip.length);
			}
		}
	}
}