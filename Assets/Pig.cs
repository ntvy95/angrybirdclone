﻿using UnityEngine;
using System.Collections;

public class Pig : MonoBehaviour {
	public Sprite PigCloseEyes_100;
	public Sprite PigOpenEyes_100;
	public Sprite PigCloseEyes_50;
	public Sprite PigOpenEyes_50;
	public Sprite PigCloseEyes_25;
	public Sprite PigOpenEyes_25;
	public Sprite PigCloseEyes;
	public Sprite PigOpenEyes;
	public GameObject myPoints;
	public float maxHP;
	[HideInInspector]
	public float HP;
	[HideInInspector]
	public bool Destroyed;
	// Use this for initialization
	void Start () {
		StartCoroutine (blinkingeyes ());
		PigCloseEyes = PigCloseEyes_100;
		PigOpenEyes = PigOpenEyes_100;
		HP = maxHP;
		Destroyed = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnCollisionEnter2D(Collision2D col) {
		if (Destroyed == false) {
			float Force = Vector2.Distance (col.relativeVelocity, new Vector2 (0, 0)),
			Damage = Force * 500f;
			HP = HP - Damage;
			if (HP <= 0) {
				Destroyed = true;
				initiatemyPoints ();
				GetComponent<AudioSource> ().Play ();
				GameManager.numberofPig = GameManager.numberofPig - 1;
				if (GameManager.numberofPig == 0) {
					CameraMove.currentMovingCameraBackState = MovingCameraBackState.MovingCameraBackEnd;
					SlingShot.currentSlingShotState = SlingShotState.End;
				}
				GetComponent<SpriteRenderer>().enabled = false;
				Destroy (gameObject, GetComponent<AudioSource>().clip.length);
				Destroy (myPoints, 0.5f);
			} else if (HP <= (0.25 * maxHP)) {
				PigCloseEyes = PigCloseEyes_25;
				PigOpenEyes = PigOpenEyes_25;
			} else if (HP <= (0.5 * maxHP)) {
				PigCloseEyes = PigCloseEyes_50;
				PigOpenEyes = PigOpenEyes_50;
			}
		}
	}

	public void initiatemyPoints() {
		GameObject MP = (GameObject)Instantiate(myPoints, transform.position, Quaternion.identity);
		MP.GetComponent<SpriteRenderer> ().enabled = true;
		myPoints = MP;
	}

	IEnumerator blinkingeyes() {
		while (true) {
			GetComponent<SpriteRenderer> ().sprite = PigOpenEyes;
			yield return new WaitForSeconds(3f);
			GetComponent<SpriteRenderer> ().sprite = PigCloseEyes;
			yield return new WaitForSeconds(1f);
		}
	}
}
