﻿using UnityEngine;
using System.Collections;

public class SlingShot : MonoBehaviour {
	public static SlingShotState currentSlingShotState;
	public LineRenderer Left;
	public LineRenderer Right;
	public AudioSource ShotSound;
	public AudioSource StretchSound;
	bool Stretching;
	// Use this for initialization
	void Start () {
		currentSlingShotState = SlingShotState.NoBird;
		Stretching = false;

		Left.SetPosition (0, Left.transform.position);
		Right.SetPosition (0, Right.transform.position);
		Left.sortingLayerName = "Sling Shot and Bird";
		Right.sortingLayerName = "Sling Shot and Bird";
		Left.sortingOrder = 4;
		Right.sortingOrder = 0;
		Set_rubbberband_state (false);
	}

	void LateUpdate () {
		switch (currentSlingShotState) {
		case SlingShotState.NoBird:
			StartCoroutine(MoveBirdtoSlingShot());
			break;
		case SlingShotState.HavingBird:
			if(Input.GetMouseButtonDown (0)) {
				Vector3 location = GameManager.MainCamera.GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);
				if(GameManager.currentBird.GetComponent<CircleCollider2D>() == Physics2D.OverlapPoint (location)) {
					GameManager.CurrentGameState = GameState.EndThrowing;
					currentSlingShotState = SlingShotState.Pulling;
					CameraMove.currentCameraMoveState = CameraMoveState.NoMoving;
					Set_rubbberband_state (true);
				}
			}
			break;
		case SlingShotState.Pulling:
			if(Input.GetMouseButtonUp (0)) {
				ShotSound.Play ();

				GameManager.currentBird.GetComponent<Bird>().OnThrow ();
				Vector3 Force = (GameManager.BirdThrowPosition.position - GameManager.currentBird.transform.position) * 12.5f;
				GameManager.currentBird.GetComponent<Rigidbody2D>().AddForce(new Vector2(Force.x, Force.y), ForceMode2D.Impulse);

				CameraFollow.currentCameraFollowState = CameraFollowState.StartFollowing;
				Set_rubbberband_state (false);
				currentSlingShotState = SlingShotState.Flying;
				GameManager.CurrentGameState = GameState.StartThrowing;
			}
			else if(Input.GetMouseButton (0)) {
				Vector3 location = GameManager.MainCamera.GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);
				location.z = 0;
				var distance = Vector3.Distance (location, GameManager.BirdThrowPosition.position);
				if(distance > 1.5f) {
					location = (location - GameManager.BirdThrowPosition.position).normalized * 1.5f + GameManager.BirdThrowPosition.position;
					if(Stretching) {
						StretchSound.Play ();
						Stretching = false;
					}
				}
				else if(distance < 0.3f) {
					location.x = location.x + 0.3f;
					Stretching = true;
				}
				else {
					Stretching = true;
				}
				GameManager.currentBird.transform.position = location;
				Vector3 position = new Vector3(location.x, location.y, location.z);
				if(location.x < GameManager.BirdThrowPosition.position.x) {
					position.x = position.x - 0.15f;
				}
				else if(location.x > GameManager.BirdThrowPosition.position.x) {
					position.x = position.x + 0.15f;
				}
				Set_rubberband_vertex(1, position);
			}
			break;
		case SlingShotState.Flying:
			if(GameManager.currentBird != null) {
				GameManager.BirdVelocity = Vector2.Distance (GameManager.currentBird.GetComponent<Rigidbody2D>().velocity, new Vector2(0, 0));
				if(GameManager.BirdVelocity < 0.2f) {
					GameManager.SwitchBird();
				}
			}
			else {
				GameManager.SwitchBird();
			}
			break;
		case SlingShotState.End:
			break;
		}
	}

	IEnumerator MoveBirdtoSlingShot() {
		while (GameManager.currentBird.transform.position.Equals (GameManager.BirdThrowPosition.position) == false) {
			GameManager.currentBird.transform.position =
			Vector3.MoveTowards (GameManager.currentBird.transform.position, GameManager.BirdThrowPosition.position, 0.002f);
			yield return null;
		}
		if (currentSlingShotState == SlingShotState.NoBird) {
			currentSlingShotState = SlingShotState.HavingBird;
			GameManager.currentBird.GetComponent<CircleCollider2D> ().radius = 0.5f;
		}
	}

	public void Set_rubberband_vertex(int index, Vector3 position) {
		Left.SetPosition (index, position);
		Right.SetPosition (index, position);
	}

	public void Set_rubbberband_state(bool state) {
		Left.enabled = state;
		Right.enabled = state;
	}
}
