﻿using UnityEngine;
using System.Collections;

public class CameraAspect : MonoBehaviour {
	public float halfwidth;
	public float halfheight;
	public float right;
	public float top;
	public float left;
	public float bottom;
	public float CameraMinX;
	public float CameraMaxX;
	// Use this for initialization
	void Start () {
		CalculateCameraSizePosition ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CalculateCameraSizePosition() {
		halfheight = GetComponent<Camera> ().orthographicSize;
		halfwidth = halfheight * Screen.width / Screen.height;
		top = transform.position.y + halfheight;
		bottom = transform.position.y - halfheight;
		left = transform.position.x - halfwidth;
		right = transform.position.x + halfwidth;
		CameraMinX = Constants.MinX + halfwidth;
		CameraMaxX = Constants.MaxX - halfwidth;
	}
}
